package org.taveren.repository;

import org.taveren.entities.Order;

import java.util.List;

public interface OrderRepository {

    Order selectById(int orderId);

    List<Order> selectAll();

    List<Order> selectOrderBy();

    void create(Order order);

    void update(Order order);

    void delete(int orderId);
}
