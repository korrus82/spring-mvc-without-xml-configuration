package org.taveren.repository;

import org.taveren.entities.Order;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class OrderRepositoryImpl implements OrderRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Order selectById(final int orderId) {
        return entityManager.find(Order.class, orderId);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Order> selectAll() {
        return (List<Order>) entityManager.createQuery("SELECT o FROM Order o").getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Order> selectOrderBy() {
        return (List<Order>) entityManager.createQuery("SELECT o FROM Order o order by o.orderId").getResultList();
    }

    @Override
    public void create(final Order order) {
        entityManager.persist(order);
    }

    @Override
    public void update(final Order order) {
        entityManager.merge(order);
    }

    @Override
    public void delete(final int orderId) {
        entityManager.remove(selectById(orderId));
    }
}