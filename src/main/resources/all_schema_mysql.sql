CREATE SCHEMA IF NOT EXISTS `ds_0`;
CREATE SCHEMA IF NOT EXISTS `ds_1`;

CREATE TABLE IF NOT EXISTS `ds_0`.`t_order_0` (
  `order_id` INT NOT NULL,
  `user_id`  INT NOT NULL,
  `status`   VARCHAR(50),
  PRIMARY KEY (`order_id`)
);
CREATE TABLE IF NOT EXISTS `ds_0`.`t_order_1` (
  `order_id` INT NOT NULL,
  `user_id`  INT NOT NULL,
  `status`   VARCHAR(50),
  PRIMARY KEY (`order_id`)
);

CREATE TABLE IF NOT EXISTS `ds_1`.`t_order_0` (
  `order_id` INT NOT NULL,
  `user_id`  INT NOT NULL,
  `status`   VARCHAR(50),
  PRIMARY KEY (`order_id`)
);
CREATE TABLE IF NOT EXISTS `ds_1`.`t_order_1` (
  `order_id` INT NOT NULL,
  `user_id`  INT NOT NULL,
  `status`   VARCHAR(50),
  PRIMARY KEY (`order_id`)
);


INSERT INTO `ds_0`.`t_order_0` VALUES (1000, 10, 'INIT');
INSERT INTO `ds_0`.`t_order_0` VALUES (1002, 10, 'INIT');
INSERT INTO `ds_0`.`t_order_0` VALUES (1004, 10, 'INIT');
INSERT INTO `ds_0`.`t_order_0` VALUES (1006, 10, 'INIT');
INSERT INTO `ds_0`.`t_order_0` VALUES (1008, 10, 'INIT');

INSERT INTO `ds_0`.`t_order_1` VALUES (1001, 10, 'INIT');
INSERT INTO `ds_0`.`t_order_1` VALUES (1003, 10, 'INIT');
INSERT INTO `ds_0`.`t_order_1` VALUES (1005, 10, 'INIT');
INSERT INTO `ds_0`.`t_order_1` VALUES (1007, 10, 'INIT');
INSERT INTO `ds_0`.`t_order_1` VALUES (1009, 10, 'INIT');

INSERT INTO `ds_1`.`t_order_0` VALUES (1100, 11, 'INIT');
INSERT INTO `ds_1`.`t_order_0` VALUES (1102, 11, 'INIT');
INSERT INTO `ds_1`.`t_order_0` VALUES (1104, 11, 'INIT');
INSERT INTO `ds_1`.`t_order_0` VALUES (1106, 11, 'INIT');
INSERT INTO `ds_1`.`t_order_0` VALUES (1108, 11, 'INIT');

INSERT INTO `ds_1`.`t_order_1` VALUES (1101, 11, 'INIT');
INSERT INTO `ds_1`.`t_order_1` VALUES (1103, 11, 'INIT');
INSERT INTO `ds_1`.`t_order_1` VALUES (1105, 11, 'INIT');
INSERT INTO `ds_1`.`t_order_1` VALUES (1107, 11, 'INIT');
INSERT INTO `ds_1`.`t_order_1` VALUES (1109, 11, 'INIT');
