CREATE DATABASE ds_0 WITH ENCODING = 'UTF8' CONNECTION LIMIT = -1;
CREATE DATABASE ds_1 WITH ENCODING = 'UTF8' CONNECTION LIMIT = -1;

CREATE TABLE t_order_0 (
  order_id INTEGER               NOT NULL,
  user_id  INTEGER               NOT NULL,
  status   CHARACTER VARYING(50) NOT NULL,
  CONSTRAINT t_order_0_pkey PRIMARY KEY (order_id)
);
CREATE TABLE t_order_1 (
  order_id INTEGER               NOT NULL,
  user_id  INTEGER               NOT NULL,
  status   CHARACTER VARYING(50) NOT NULL,
  CONSTRAINT t_order_1_pkey PRIMARY KEY (order_id)
);
CREATE TABLE t_order_item_0 (
  item_id  INTEGER NOT NULL,
  order_id INTEGER NOT NULL,
  user_id  INTEGER NOT NULL,
  CONSTRAINT t_order_item_0_pkey PRIMARY KEY (item_id)
);
CREATE TABLE t_order_item_1 (
  item_id  INTEGER NOT NULL,
  order_id INTEGER NOT NULL,
  user_id  INTEGER NOT NULL,
  CONSTRAINT t_order_item_1_pkey PRIMARY KEY (item_id)
);

--ds_0
INSERT INTO t_order_0 (order_id, user_id, status) VALUES
  (1000, 10, 'INIT'),
  (1002, 10, 'INIT'),
  (1004, 10, 'INIT'),
  (1006, 10, 'INIT'),
  (1008, 10, 'INIT');

INSERT INTO t_order_item_0 (item_id, order_id, user_id) VALUES
  (100001, 1000, 10),
  (100201, 1002, 10),
  (100401, 1004, 10),
  (100601, 1006, 10),
  (100801, 1008, 10);

INSERT INTO t_order_1 (order_id, user_id, status) VALUES
  (1001, 10, 'INIT'),
  (1003, 10, 'INIT'),
  (1005, 10, 'INIT'),
  (1007, 10, 'INIT'),
  (1009, 10, 'INIT');

INSERT INTO t_order_item_1 (item_id, order_id, user_id) VALUES
  (100101, 1001, 10),
  (100301, 1003, 10),
  (100501, 1005, 10),
  (100701, 1007, 10),
  (100901, 1009, 10);

--ds_1
INSERT INTO t_order_0 (order_id, user_id, status) VALUES
  (1100, 11, 'INIT'),
  (1102, 11, 'INIT'),
  (1104, 11, 'INIT'),
  (1106, 11, 'INIT'),
  (1108, 11, 'INIT');

INSERT INTO t_order_item_0 (item_id, order_id, user_id) VALUES
  (110001, 1100, 11),
  (110201, 1102, 11),
  (110401, 1104, 11),
  (110601, 1106, 11),
  (110801, 1108, 11);

INSERT INTO t_order_1 (order_id, user_id, status) VALUES
  (1101, 11, 'INIT'),
  (1103, 11, 'INIT'),
  (1105, 11, 'INIT'),
  (1107, 11, 'INIT'),
  (1109, 11, 'INIT');

INSERT INTO t_order_item_1 (item_id, order_id, user_id) VALUES
  (110101, 1101, 11),
  (110301, 1103, 11),
  (110501, 1105, 11),
  (110701, 1107, 11),
  (110901, 1109, 11);